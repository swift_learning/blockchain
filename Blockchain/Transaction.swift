//
//  Transaction.swift
//  Blockchain
//
//  Created by iulian david on 05/10/2019.
//  Copyright © 2019 Edy. All rights reserved.
//

import Foundation

enum TransactionError: Error {
    case accountError(String)
    case invalidTransaction(String)
}

extension TransactionError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .accountError(let message):
            let format = NSLocalizedString(
                "Account Error: '%@'",
                comment: ""
            )

            return String(format: format, message)
        case .invalidTransaction(let message):
            let format = NSLocalizedString(
                "Transaction did not complete. Reason: '%@'",
                comment: ""
            )

            return String(format: format, message)
        }
    }
}

class Transaction {

    private var accounts = ["0000": 1_000_000]
    private let bitcoinChain = Blockchain()
    private let reward = 150

    func transaction(from fromParty: String, to partner: String, amount: Int, handler: @escaping (TransactionError?) -> Void) {

        guard let fromAccountAmount = accounts[fromParty] else {
            handler(TransactionError.accountError("No source found"))
            return
        }

        guard fromAccountAmount - amount > 0 else {
            handler(TransactionError.invalidTransaction("Not enough coins to complete the transaction"))
            return
        }

        accounts.updateValue(fromAccountAmount - amount, forKey: fromParty)
        accounts[partner, default: 0] += amount

        bitcoinChain.createBlock(from: fromParty, to: partner, amount: amount)
    }

    func isChainValid() -> String {
        return "Chain is valid: \(bitcoinChain.isValid())"
    }

    func chainDescription() -> String {
        return bitcoinChain.description
    }

    func balance(for account: Int) -> Int {
        return accounts[String(account)] ?? 0
    }
}
