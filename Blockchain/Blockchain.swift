//
//  Blockchain.swift
//  Blockchain
//
//

import UIKit

class Blockchain {
    //logic for the Blockchain here
    private var chain = [Block]()

    func createInitialBlock(data: String) {
        let initialBlock = Block(data: data, previousHash: "0000", index: 0)
        chain.append(initialBlock)
    }

    func createBlock(from source: String, to destination: String, amount: Int) {
        if chain.isEmpty {
            createInitialBlock(data: "From: \(source); To: \(destination)")
        } else {
            createBlock(data: "From: \(source); To: \(destination); Amount: \(amount)")
        }
    }

    func createBlock(data: String) {
        let previousBlock = chain[chain.count - 1]
        let block = Block(data: data, previousHash: previousBlock.hash, index: chain.count)
        chain.append(block)
    }

    func isValid() -> Bool {
        var isValid = true

        for (index, block) in chain.enumerated()
            where index > 0 && block.previousHash != chain[ index - 1 ].hash {
            isValid = false
            break
        }
        return isValid
    }
}

extension Blockchain: CustomStringConvertible {
    var description: String {
//        return chain.reduce(into: "") { (result, block) in
//            result += "\n\(block.description)"
//        }
        return chain.reduce(into: "\nChain") { $0 += "\n\($1.description)" }
    }
}
