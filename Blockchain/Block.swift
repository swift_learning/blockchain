//
//  Block.swift
//  Blockchain
//
//

import UIKit

struct Block {
    // logic for Block here
    let hash: String
    let data: String
    let previousHash: String
    let index: Int

    init(data: String, previousHash: String, index: Int) {
        self.hash = UUID().uuidString.replacingOccurrences(of: "-", with: "")
        self.data = data
        self.previousHash = previousHash
        self.index = index
    }

    func generateHash() -> String {
        return UUID().uuidString.replacingOccurrences(of: "-", with: "")
    }

}

extension Block: CustomStringConvertible {
    var description: String {
        "Block \(index)\n\tHash:\(hash)\n\tPreviousHash:\(previousHash)\n\tData:\(data)"
    }
}
