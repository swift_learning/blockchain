//
//  ViewController.swift
//  Blockchain
//

import UIKit

class ViewController: UIViewController {

    let transaction = Transaction()
    private let firstAccount = 1010
    private let secondAccount = 1011

    @IBOutlet weak var greenAmount: UITextField!
    @IBOutlet weak var redAmount: UITextField!
    @IBOutlet weak var redLabel: UILabel!
    @IBOutlet weak var greenLabel: UILabel!
    @IBOutlet weak var firstAccountLabel: UILabel!
    @IBOutlet weak var secondAccountLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        firstAccountLabel.text = "Account: \(firstAccount)"
        secondAccountLabel.text = "Account: \(secondAccount)"
        // Do any additional setup after loading the view, typically from a nib.
        transaction(from: "0000", to: "\(firstAccount)", amount: 50)
        transaction(from: "\(firstAccount)", to: "\(secondAccount)", amount: 10)
    }

    fileprivate func createAlert(_ transactionError: (TransactionError)) {
        let alertController = UIAlertController(title: "Invalid Transaction", message: transactionError.localizedDescription, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }

    func transaction(from fromParty: String, to partner: String, amount: Int) {
        transaction.transaction(from: fromParty, to: partner, amount: amount) { [unowned self] (result: TransactionError?) in
            if let error = result {
                self.createAlert(error)
            } else {
                print("Success!")
            }
        }
        chainState()
    }

    func chainState() {
        print(transaction.chainDescription())
        redLabel.text = "Balance: \(transaction.balance(for: firstAccount)) BTC"
        greenLabel.text = "Balance: \(transaction.balance(for: secondAccount)) BTC"
    }

    func chainValidity() -> String {
        return transaction.isChainValid()
    }

    @IBAction func redMine(_ sender: Any) {
        transaction(from: "0000", to: "\(firstAccount)", amount: 150)
    }

    @IBAction func greenMine(_ sender: Any) {
        transaction(from: "0000", to: "\(secondAccount)", amount: 150)
    }

    @IBAction func redSend(_ sender: Any) {
        guard let amount = Int(redAmount.text ?? ""), amount > 0 else {
            createAlert(TransactionError.invalidTransaction("Invalid amount"))
            return
        }
        transaction(from: "\(firstAccount)", to: "\(secondAccount)", amount: amount)
        redAmount.text = ""
    }

    @IBAction func greenSend(_ sender: Any) {
        guard let amount = Int(greenAmount.text ?? ""), amount > 0 else {
            createAlert(TransactionError.invalidTransaction("Invalid amount"))
            return
        }
        transaction(from: "\(secondAccount)", to: "\(firstAccount)", amount: amount)
        greenAmount.text = ""
    }

}

extension ViewController {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
